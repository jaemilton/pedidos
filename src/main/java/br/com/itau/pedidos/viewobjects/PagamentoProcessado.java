package br.com.itau.pedidos.viewobjects;

public class PagamentoProcessado extends Pagamento {

	private boolean autorizado;
	
	public boolean isAutorizado() {
		return autorizado;
	}
	public void setAutorizado(boolean autorizado) {
		this.autorizado = autorizado;
	}
}
