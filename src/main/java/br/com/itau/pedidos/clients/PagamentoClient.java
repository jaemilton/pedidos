package br.com.itau.pedidos.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.itau.pedidos.viewobjects.Curso;

@FeignClient(name="pagamento")
public interface PagamentoClient {

	@PostMapping
	public Curso buscarPorNome(@PathVariable String nome);
}
