package br.com.itau.pedidos.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.pedidos.viewobjects.Curso;

@FeignClient(name="catalogo")
public interface CursoClient {

	@GetMapping("catalogo/{nome}")
	public Curso buscarPorNome(@PathVariable String nome);
}
