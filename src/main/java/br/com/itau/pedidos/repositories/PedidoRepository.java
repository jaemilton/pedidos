package br.com.itau.pedidos.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.pedidos.models.Pedido;

public interface PedidoRepository  extends CrudRepository<Pedido, Integer> {
	
}
