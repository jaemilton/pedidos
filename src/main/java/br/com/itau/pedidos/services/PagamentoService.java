package br.com.itau.pedidos.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.viewobjects.Pagamento;
import br.com.itau.pedidos.viewobjects.PagamentoProcessado;

@Service
public class PagamentoService {

	@Autowired
	KafkaTemplate<Object, Object> kafkaTemplate;
	
	@Autowired
	PedidoService pedidoService;

	public void publicar(Pagamento pagamento) {
		kafkaTemplate.send("PedidosPendentes", pagamento);
	}
	
	@Bean
	public RecordMessageConverter converter()
	{
		return new StringJsonMessageConverter();
	}
	
	@KafkaListener(id="pagamentoprocessado", topics="PedidosProcessados")
	public void consumirPagamento(@Payload PagamentoProcessado pagamento) throws Exception
	{
		Optional<Pedido> pedido =
				pedidoService.buscarPorId(pagamento.getIdPedido());
		
		if (!pedido.isPresent())
		{
			throw new Exception("Pedido não encontrado."); 
		}
		
		pedido.get().setStatusPagamento(pagamento.isAutorizado());
		
		pedidoService.atualizar(pedido.get());
		
	}

}
