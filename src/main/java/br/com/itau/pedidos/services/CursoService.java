package br.com.itau.pedidos.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.itau.pedidos.clients.CursoClient;
import br.com.itau.pedidos.viewobjects.Curso;

@Service
public class CursoService {

	@Autowired
	CursoClient cursoClient;
	
	public Optional<Curso> buscar(String cpf){
		Curso curso;
		try {
			curso = cursoClient.buscarPorNome(cpf);
	
		} catch (Exception e) {
			return Optional.empty();
		}
		
		return Optional.of(curso);
	}
}
