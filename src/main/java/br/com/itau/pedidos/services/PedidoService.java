package br.com.itau.pedidos.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.repositories.PedidoRepository;

@Service
public class PedidoService {

	@Autowired
	PedidoRepository pedidoRepository;
	
	public Pedido cadastrar(Pedido pedido)
	{
		return pedidoRepository.save(pedido);
	}
	
	public Optional<Pedido> buscarPorId(int id)
	{
		return pedidoRepository.findById(id);
	}
	
	public Iterable<Pedido> listar()
	{
		return pedidoRepository.findAll();
	}
	
	public Pedido atualizar(Pedido pedido)
	{
		return pedidoRepository.save(pedido);
	}
	
}
