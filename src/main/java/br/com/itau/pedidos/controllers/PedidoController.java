package br.com.itau.pedidos.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.services.CursoService;
import br.com.itau.pedidos.services.PagamentoService;
import br.com.itau.pedidos.services.PedidoService;
import br.com.itau.pedidos.viewobjects.Curso;
import br.com.itau.pedidos.viewobjects.Pagamento;

@RestController
@RequestMapping
public class PedidoController {

	@Autowired
	PedidoService pedidoService;
	
	@Autowired
	CursoService cursoService;
	
	@Autowired
	PagamentoService pagamentoService;
	
	@PostMapping
	public ResponseEntity cadastrar(@RequestBody Pedido pedido){
		
		Optional<Curso> custoOptional = 
				cursoService.buscar(pedido.getNomeCurso());
		
		if (
				!custoOptional.isPresent() ||
				(
					custoOptional.isPresent() && 
					!custoOptional.get().isDisponivel()
				)
			)
		{
			return ResponseEntity.notFound().build();
		}
		
		pedido.setPrecoPago(custoOptional.get().getPreco());
		pedido = pedidoService.cadastrar(pedido);
		
		Pagamento pagamento = new Pagamento();
		pagamento.setIdPedido(pedido.getId());
		pagamento.setPrecoPago(pedido.getPrecoPago());
		
		pagamentoService.publicar(pagamento);
		
		return ResponseEntity.status(201).body(pedido);
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity buscar(@PathVariable int id) {
		
		Optional<Pedido> pedidoOptional = pedidoService.buscarPorId(id);
		
		if(pedidoOptional.isPresent()) {
			return ResponseEntity.ok(pedidoOptional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping
	public Iterable<Pedido> listar(){
		return pedidoService.listar();
	}

}
